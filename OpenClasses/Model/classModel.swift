//
//  classModel.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

struct classModel: Decodable {
    var id: Int?
    var title: String?
    var date: String?
    var time: String?
    var duration: String?
    var cost: Int?
    var location: String?
    var teacher: String?
}
