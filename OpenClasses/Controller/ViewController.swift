//
//  ViewController.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var classesButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var mySessionsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        UIView.animate(withDuration: 1.5) {
            self.logo.isHidden = false
            UIView.animate(withDuration: 1) {
                self.classesButton.isHidden = false
                self.mySessionsButton.isHidden = false
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loadClasses(_ sender: Any) {
        guard let classesViewController = self.storyboard?.instantiateViewController(withIdentifier: "classesViewController") as? ClassesViewController else {
            return
        }
        classesViewController.isMySessionsCall = false
        self.navigationController?.pushViewController(classesViewController, animated: true)
    }
    
    @IBAction func loadMyClasses(_ sender: Any) {
        guard let classesViewController = self.storyboard?.instantiateViewController(withIdentifier: "classesViewController") as? ClassesViewController else {
            return
        }
        classesViewController.isMySessionsCall = true
        self.navigationController?.pushViewController(classesViewController, animated: true)
    }
    
    
    
}

