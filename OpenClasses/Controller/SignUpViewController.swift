//
//  SignUpViewController.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import TextFieldEffects

class SignUpViewController: UIViewController {
    var classModel: classModel?
    @IBOutlet weak var emailTextField: YoshikoTextField!
    @IBOutlet weak var numberTextField: YoshikoTextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var codeTextLabel: YoshikoTextField!
    @IBOutlet weak var verifyButton: UIButton!
    
    var phoneNumner: String?
    var email: String?
    private var finalCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        numberTextField.delegate = self
        signUpButton.alpha = 0.5
        let tap = UITapGestureRecognizer(target: self, action: #selector(backgroundTapped))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        codeTextLabel.isHidden = true
        verifyButton.isHidden = true
        verifyButton.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signUpButtonClicked(_ sender: Any) {
        sendCode(toPhoneNumber: self.phoneNumner)
    }
    
    @IBAction func verifyButtonClicked(_ sender: Any) {
        self.finalCode = codeTextLabel.text
        guard let code = finalCode else { return }
        guard let email = self.email else { return }
        AuthService.instance.auth(code: code, userEmail: email) { (status, error) in
            if error != nil || status == false {
                print(error?.localizedDescription ?? "")
            } else if error == nil && status == true {
                guard let classDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailsViewController") as? ClassDetailsViewController else { return }
                classDetailsViewController.model = self.classModel
                self.navigationController?.pushViewController(classDetailsViewController, animated: true)
            }
        }
    }
    
    
    @objc func backgroundTapped() {
        view.endEditing(true)
    }
    
    func sendCode(toPhoneNumber number: String?) {
        guard let phoneNumber = number else { return }
        AuthService.instance.sendCode(withPhoneNumber: phoneNumber) { (status, error) in
            if error != nil || status == false {
                //present alert
                print(error?.localizedDescription ?? "")
            }
        }
        self.verifyButton.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3) {
            self.verifyButton.isHidden = false
            self.codeTextLabel.isHidden = false
        }
    }
    
}

extension SignUpViewController: UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let maxLength = text.count + string.count - range.length
        if maxLength == 10 {
            signUpButton.alpha = 1.0
        }
        if textField.tag == 2 {
            return maxLength <= 10
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(numberTextField.text?.isEmpty == true || emailTextField.text?.isEmpty == true) {
            signUpButton.alpha = 0.5
            signUpButton.isUserInteractionEnabled = false
        } else {
            signUpButton.isUserInteractionEnabled = true
            signUpButton.alpha = 1.0
            phoneNumner = "+1" + (numberTextField.text ?? "")
            email = self.emailTextField.text?.trimmingCharacters(in: .whitespaces) ?? ""
        }
    }
}
