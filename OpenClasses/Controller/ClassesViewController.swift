//
//  ClassesViewController.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ClassesViewController: UIViewController {
    var classesModelArray: [classModel] = []
    var isMySessionsCall: Bool = false
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.delegate = self
        myTableView.dataSource = self
        APIService.shared.fetchClasses(isMySessionCall: isMySessionsCall) { (result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let classes):
                self.classesModelArray = classes
                DispatchQueue.main.async {
                    self.myTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ClassesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if classesModelArray.count > 0 {
            return classesModelArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "classCell", for: indexPath) as? classCell {
            cell.selectionStyle = .none
            cell.classModel = self.classesModelArray[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //check if user has signed up before
        if Auth.auth().currentUser?.uid == nil {
            guard let signUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else { return }
            signUpViewController.classModel = classesModelArray[indexPath.row]
            self.navigationController?.pushViewController(signUpViewController, animated: true)
        } else {
            guard let classDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailsViewController") as? ClassDetailsViewController else { return }
            classDetailsViewController.isMySessionsSender = isMySessionsCall
            classDetailsViewController.model = classesModelArray[indexPath.row]
            self.navigationController?.pushViewController(classDetailsViewController, animated: true)
        }
    }
}
