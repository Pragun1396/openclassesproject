//
//  OrderConfirmViewController.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/2/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit
import TextFieldEffects

class OrderConfirmViewController: UIViewController, UIGestureRecognizerDelegate {
    var model: classModel?
    @IBOutlet weak var reserveButton: UIButton!
    @IBOutlet weak var fullNameTextField: YoshikoTextField!
    @IBOutlet weak var numberTextField: YoshikoTextField!
    @IBOutlet weak var cardNumberTextField: YoshikoTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(backgroundTapped))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backgroundTapped() {
        view.endEditing(true)
    }
    
    @IBAction func reserveButtonClicked(_ sender: Any) {
        if fullNameTextField.text?.isEmpty ?? false || numberTextField.text?.isEmpty ?? false || cardNumberTextField.text?.isEmpty ?? false {
            //present alert
        } else {
            createPostRequest()
        }
    }
    
    func createPostRequest() {
        guard let classModel = model else { return }
        APIService.shared.bookSession(withModel: classModel) { (result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(true):
                let alert = UIAlertController(title: "Alert", message: "Post request successfull", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                      switch action.style{
                      case .default:
                            print("default")
                      case .cancel:
                            print("cancel")
                      case .destructive:
                            print("destructive")
                      @unknown default:
                        break
                    }}))
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                        guard let viewController = self.storyboard?.instantiateViewController(identifier: "viewController") as? ViewController else { return }
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                }

            case .success(false):
                print("UNSUCCESSFULL POST REQUEST")
            }
        }
    }
}
