//
//  ClassDetailsViewController.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit

class ClassDetailsViewController: UIViewController {
    var model: classModel?
    var isMySessionsSender: Bool = false
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.detailsLabel.text = self.model?.title
            self.teacherLabel.text = self.model?.teacher
            self.dateAndTimeLabel.text = [self.model?.date, self.model?.time].compactMap { $0 }.joined(separator: " ")
            self.costLabel.text = "\(self.model?.cost ?? 0)"
            self.locationLabel.text = self.model?.location
            self.durationLabel.text = self.model?.duration
            if self.isMySessionsSender {
                self.addToCartButton.isHidden = true
            } else {
                self.addToCartButton.isHidden = false
            }
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func proceedToPaymentsViewController(_ sender: Any) {
        guard let orderConfirmViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmViewController") as? OrderConfirmViewController else { return }
            orderConfirmViewController.model = self.model
            self.navigationController?.pushViewController(orderConfirmViewController, animated: true)
    }
    
}
