//
//  classCell.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import UIKit

class classCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        self.layer.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
    }
    
    var classModel: classModel? {
        didSet {
            DispatchQueue.main.async {
                self.timeLabel.text = self.classModel?.time
                self.titleLabel.text = self.classModel?.date
                self.durationLabel.text = self.classModel?.duration
                self.subtitleLabel.text = self.classModel?.title
            }
        }
    }
}
