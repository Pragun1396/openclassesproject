//
//  APIService.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

class APIService: NSObject {
    static let shared = APIService()
    func fetchClasses(isMySessionCall: Bool, completion: @escaping (Result<[classModel], Error>) -> ()) {
        let urlString = isMySessionCall ? getMyClassesURL : getClassesURL
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8) ?? "")
            do {
                let classes = try JSONDecoder().decode([classModel].self, from: data)
                completion(.success(classes))
            } catch let jsonError {
                print(jsonError.localizedDescription)
                completion(.failure(jsonError))
            }
        }.resume()
    }
    
    func bookSession(withModel model: classModel, completion: @escaping (Result<Bool, Error>) -> ()) {
        guard let url = URL(string: postURL) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let parameters: [String: Any] = [
            "id": model.id as Any,
            "title": model.title as Any,
            "date": model.date as Any,
            "time": model.time as Any,
            "duration": model.duration as Any,
            "cost": model.cost as Any,
            "location": model.location as Any,
            "teacher": model.teacher as Any
        ]
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            if error != nil {
                completion(.failure(error!))
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else { return }
            print(String(data: jsonData, encoding: .utf8)!)
            completion(.success(true))
            }).resume()
    }
    
    
}
