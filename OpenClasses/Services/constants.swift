//
//  constants.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/2/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation

let getClassesURL = "http://10.0.0.64:1337/classes"
let postURL = "http://10.0.0.64:1337/bookSession"
let getMyClassesURL = "http://10.0.0.64:1337/myclasses"
