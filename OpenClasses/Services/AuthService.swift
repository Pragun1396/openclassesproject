//
//  AuthService.swift
//  OpenClasses
//
//  Created by Pragun Sharma on 2/1/20.
//  Copyright © 2020 Pragun Sharma. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class AuthService: NSObject {
    static let instance = AuthService()
    private var phoneAuthCredential: PhoneAuthCredential?
    let defaults = UserDefaults.standard

    func sendCode(withPhoneNumber phoneNumber: String, messageSentComplete: @escaping(_ status: Bool, _ error: Error?) -> ()) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                messageSentComplete(false, error)
            } else {
                self.defaults.set(verificationID, forKey: "authVID")
                self.defaults.synchronize()
                messageSentComplete(true, nil)
                
            }
        }
    }
    
    func auth(code: String, userEmail: String, authorizationComplete: @escaping(_ status: Bool, _ error: Error?) -> ()) {
        guard let verificationID = defaults.string(forKey: "authVID") else {
            authorizationComplete(false, nil)
            return
        }
        phoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: code.trimmingCharacters(in: .whitespaces))
        if let pac = phoneAuthCredential {
            Auth.auth().signIn(with: pac) { (user, error) in
                if error != nil {
                    authorizationComplete(false, error)
                } else {
                    //Authorization successfull
                    let userData: [String: String] = ["Email": userEmail]
                    guard let user = Auth.auth().currentUser else {
                        return
                    }
                    DataService.instance.createDBUserProfile(uid: user.uid, userData: userData)
                    authorizationComplete(true, nil)
                }
            }
        }
    }
}
