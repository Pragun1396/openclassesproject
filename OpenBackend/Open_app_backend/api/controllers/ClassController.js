const class1 = {id: 1, title: "Breath + Sound w/ Peter", date: "Tomorrow", time: "6 pm", duration: "60 min", cost: 30, location: "66 Sanchez St", teacher: "Peter"}
const class2 = {id: 2, title: "Breath + Sound w/ Raed", date: "Tuesday 2/4", time: "7 pm", duration: "60 min", cost: 30, location: "Mission St", teacher: "Raed" }
const class3 = {id: 3, title: "Breath + Sound w/ Erin", date: "Thursday 2/6", time: "7 pm", duration: "60 min", cost: 45, location: "One Sasome St", teacher: "Erin"}
const class4 = {id: 4, title: "Breath + Sound w/ Hailey", date: "Sunday 2/9", time: "6 pm", duration: "60 min", cost: 50, location: "South San Francisco", teacher: "Hailey"}
const mysessions = []
module.exports = {
    classes: function(req, res) {
        res.send([class1, class2, class3, class4])
    },
    bookSession: function(req,res) {
        const mySession = {id: req.body.id, title: req.body.title, date: req.body.date, time: req.body.time, duration: req.body.duration, cost: req.body.cost, location: req.body.location, teacher: req.body.teacher}
        mysessions.push(mySession)
        sails.log.debug("Teacher: " + req.body.teacher)
        res.end()
    },
    myclasses: function(req,res) {
        res.send(mysessions)
    }
}